﻿# README

Nous avons réalisé notre projet sous windows en utilisant Visual Studio 2017.
Voici les deux bibliothèques utilisées :
- SDL2 (https://www.libsdl.org/download-2.0.php)
- GLEW (http://glew.sourceforge.net/)

# Propriétés de configuration
- Général
    - Version du Windows SDK : **10.0.17134.0**
    - Ensemble d'outils de plateforme : **Visual Studio 2017 (v141)**
    - Type de configuration : **Application (.exe)**
    - Jeu de caractères : **Utiliser le jeu de caractères multioctet (MBCS)**
- Répertoires VC++
    - Répertoires Include : (ajouter) **GLEW\include ; SDL2\include**
- C/C++
    - Général
        - Niveau d'avertissement : **Niveau3 (/W3)**
        - Vérifications SDL : **Oui (/sdl)**
    - Optimisation
        - Optimisation : **Désactivé (/Od)**
    - Langage
        - Mode de onformité : **Oui (/permissive-)**
- Éditeur de liens
    - Général
        - Répertoires de bibliothèques supplémentaires : (ajouter) **Glew\lib\Release\Win32 ; \SDL2\lib\x86**
    - entrée
        - Dépendances supplémentaires : (ajouter) **SDL2.lib ; SDLmain.lib ; opengl32.lib ; glu32.lib**
    - Système
        - Sous-système : **Windows (/SUBSYSTEM:WINDOWS)**

# Fichiers DLL

Ajouter, dans le dossier debug de la hierarchie du projet visual, les fichiers : **glew32.dll** ainsi que **SDL2.dll**.

# Réalisations

Nous avons réalisé une structure de code permettant d'afficher une scène 3D contenant plusieurs objets autour desquels se déplace une caméra. La scène est éclairée par des lumières ambiantes, diffuses et spéculaires. De plus, des textures sont appliquées sur les objets.

Nous n'avons pas mis en place les vbo/vao pour ne pas perdre les fonctionnalités comme l'éclairage et les textures. Nous voulions pas, par manque de temps, risquer de régresser sur notre avancement.