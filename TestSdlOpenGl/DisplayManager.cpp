/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: DisplayManager.cpp                                                 * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#include "Modele.h"
#include "DisplayManager.h"
#include "RenderingGlobalRoutine.h"
#include "ICamera.h"
#include "FramesData.h"

DisplayManager::DisplayManager():
  mWindowChanged(true)
{
	RenderingGlobalRoutine::Init();
}

void DisplayManager::setWindowChanged() {
  mWindowChanged = true;
}

void DisplayManager::Affichage(const Modele& modele){
  // Si les dimensions de la fenêtre ont changé (ou à l'initialisation)
  if (mWindowChanged){
    Redimensionnement(modele); // (re-)Initialisation de la projection 3D --> 2D
    mWindowChanged = false;
  }
	// On efface le buffer vidéo (fenêtre graphique)
	/*float niveauGris = modele.getNiveauGris();
	glClearColor(niveauGris, niveauGris, niveauGris, 1.0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	*/

	RenderingGlobalRoutine::InitView();
	ICamera::ClearModelView();
	modele.getLightSources().ApplyLightPositions(LightSourceData::TypeRepere::CAMERA);
	modele.getCamera().ApplyPerspectiveProjection();
	modele.getCamera().ApplyCameraCoordinates();
	modele.ApplyModelTransform();
	RenderingGlobalRoutine::DrawModel(modele);
	modele.getLightSources().ApplyLightPositions(LightSourceData::TypeRepere::MONDE);
	modele.getLightSources().ApplyLightIntensities();
	RenderingGlobalRoutine::ApplyMaterial(modele.getDefaultMaterial());
}

void DisplayManager::Redimensionnement(const Modele& modele){
	// Surface de rendu (voir chapitres suivants)
	glViewport(0,0,(GLsizei)modele.getLargeurFenetre(),
						 (GLsizei)modele.getHauteurFenetre());
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, modele.getLargeurFenetre(), 0, modele.getHauteurFenetre(), 1, 600);
}
