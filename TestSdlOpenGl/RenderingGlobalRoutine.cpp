#include "RenderingGlobalRoutine.h"
#include "Modele.h"
#include "Material.h"

#include <string>
#include <fstream>

#include <Windows.h>
#include <GL/glew.h>
#include <GL/GL.h>

const char * vertexFile = "../shaders/vertexShader";
const char * fragmentFile = "../shaders/fragmentShader";

GLuint shader[2];
GLuint program;

std::string getFileString(const char * file)
{
	std::ifstream ifs(file);
		return std::string((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
}

void RenderingGlobalRoutine::Init()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glColor3f(1.0f, 1.0f, 1.0f);
	//glLineWidth(2);
	glPolygonMode(GL_FRONT, 0);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_LIGHTING);

	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

	glShadeModel(GL_SMOOTH);

	GLfloat ambient_scene[] = { 0.2, 0.2, 0.2, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_scene);

	GLfloat ambient[] = { 1.0, 1.0, 1.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	
	glewInit();

	GLint isCompiled = 0;
	std::string tmp = getFileString(vertexFile);
	GLchar *vertexSource = (GLchar *)tmp.c_str();
	shader[0] = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shader[0], 1, &vertexSource, 0);
	glCompileShader(shader[0]);

	glGetShaderiv(shader[0], GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader[0], GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infolog(maxLength);
		glGetShaderInfoLog(shader[0], maxLength, &maxLength, &infolog[0]);

		glDeleteShader(shader[0]);
		exit(- 1);
	}


	tmp = getFileString(fragmentFile);
	GLchar *fragmentSource = (GLchar *)tmp.c_str();
	shader[1] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shader[1], 1, &fragmentSource, 0);
	glCompileShader(shader[1]);

	glGetShaderiv(shader[1], GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader[1], GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infolog(maxLength);
		glGetShaderInfoLog(shader[1], maxLength, &maxLength, &infolog[0]);


		for (unsigned i = 0; i < 2; i++)
			glDeleteShader(shader[i]);
		exit(-2);
	}

	program = glCreateProgram();

	for (unsigned i = 0; i < 2; i++)
		glAttachShader(program, shader[i]);

	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infolog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infolog[0]);

		glDeleteProgram(program);

		for (unsigned i = 0; i < 2; i++)
			glDeleteShader(shader[i]);
		exit(-3);
	}


	for (unsigned i = 0; i < 2; i++)
		glDetachShader(program, shader[i]);

}

void RenderingGlobalRoutine::InitView()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderingGlobalRoutine::ApplyPointLightPosition(int lightId, const float position[4])
{
	glLightfv(lightId, GL_POSITION, position);
}

void RenderingGlobalRoutine::ApplyPointLightIntensity(int lightId, const float diffuseIntensity[4], const float specularIntensity[4])
{
	glLightfv(lightId, GL_DIFFUSE, diffuseIntensity);
	glLightfv(lightId, GL_SPECULAR, specularIntensity);
	glEnable(lightId);
}

void RenderingGlobalRoutine::DisablePointLight(int lightId)
{
	glDisable(lightId);
}

void RenderingGlobalRoutine::ApplyMaterial(Material material)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, material.getAmbient());
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material.getDiffuse());
	glMaterialfv(GL_FRONT, GL_SPECULAR, material.getSpecular());
	glMaterialf(GL_FRONT, GL_SHININESS, material.getShininess());
}

void RenderingGlobalRoutine::DrawModel(const Modele & modele)
{
	glEnable(GL_TEXTURE_2D);

	modele.getTexture().SelectTexture2D();
	
	glBegin(GL_TRIANGLES);
	//glUseProgram(program);

	for (int32_t i = 0; i < modele.getNbFaces(); ++i)
	{
		for (unsigned j = 0; j < 3; ++j)
		{
			glTexCoord2fv(modele.getTexturePos(modele.getFaceTexture(i)[j]));
			glVertex3fv(modele.getVertexPos(modele.getFace(i)[j]));
		}
	}

	//glUseProgram(0);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}
