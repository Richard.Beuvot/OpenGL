#pragma once

#include <vector>
#include "RenderingGlobalRoutine.h"

class LightSourceData
{
public:
	class PointLightSource
	{
	public :

		PointLightSource(int lightId,
						float lightPositionX, float lightPositionY, float lightPositionZ,
						float diffuseIntensityR, float diffuseIntensityG, float diffuseIntensityB,
						float specularIntensityR, float specularIntensityG, float specularIntensityB );

		void ApplyPosition() const;
		void ApplyIntensity() const;
		void Disable() const;

		int getLightId() const;

		const float * getLightPosition() const;
		const float * getDiffuseIntensity() const;
		const float * getSpecularIntensity() const;

	private:
		int mLightId;

		float mLightPosition[4] = { 0.0f, 0.0f, 10.0f, 0.0f };
		float mDiffuseIntensity[4] = { 0.6f, 0.6f, 0.6f, 1.0f };
		float mSpecularIntensity[4] = { 0.6f, 0.6f, 0.6f, 1.0f };
	};

	enum class TypeRepere
	{
		MONDE = 0, CAMERA = 1
	};

	LightSourceData();

	bool AddSource(LightSourceData::TypeRepere typeRepere, int lightId,
					float lightPositionX, float lightPositionY, float lightPositionZ,
					float diffuseIntensityR, float diffuseIntensityG, float diffuseIntensityB,
					float specularIntensityR, float specularIntensityG, float specularIntensityB);

	bool DeleteSource(int lightId);

	void ApplyLightPositions(LightSourceData::TypeRepere typeRepere) const;

	void ApplyLightIntensities() const;

	void DisableLightSources(LightSourceData::TypeRepere typeRepere);

private :

	std::vector<LightSourceData::PointLightSource>& GetSourcesByRepere(LightSourceData::TypeRepere typeRepere,
																		bool reverse = false);
	const std::vector<LightSourceData::PointLightSource>& GetSourcesByRepere(LightSourceData::TypeRepere typeRepere,
																		bool reverse = false) const;

	std::vector<PointLightSource> mSourcesRepereCamera;
	std::vector<PointLightSource> mSourcesRepereMonde;
};

