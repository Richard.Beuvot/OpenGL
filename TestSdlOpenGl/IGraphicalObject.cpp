#include "IGraphicalObject.h"

IGraphicalObject::IGraphicalObject()
{
	vertices.push_back(Point(0.5714285714285714, 0.5714285714285714, -0.0142857142857143));
	vertices.push_back(Point(1.0, 1.0, -0.0142857142857143));
	vertices.push_back(Point(0.0, 0.7142857142857143, -0.0142857142857143));
	vertices.push_back(Point(0.7142857142857143, 0.0, -0.7142857142857143));
	// Multiply by 700
	// Automate Point normalization when constructing it ?
	faces.push_back(Face(0, 1, 2));
	faces.push_back(Face(0, 1, 3));
	faces.push_back(Face(0, 2, 3));
	faces.push_back(Face(1, 2, 3));
}


IGraphicalObject::~IGraphicalObject()
{
}
