#pragma once

#include <vector>

class PixelsBuffer
{
public:
	PixelsBuffer(int, int, int);
	PixelsBuffer(int, int, int, unsigned char*);

	void setSize(int, int, int);

	int GetWidth();

	int GetHeight();

	int GetBytesPerPixel();

	void SetPixelPerBytes(int, int, unsigned char *);

	unsigned char * GetRawData();

	~PixelsBuffer();

private:
	int mWidth;
	int mHeight;
	int mBytesPerPixel;
	int mScanLineWidth;
	std::vector<unsigned char> mPixels;

	PixelsBuffer();
};

