#include "Scene.h"

#include <GL/glew.h>
#include <GL/GL.h>

Scene::Scene() :
	mDefaultMaterial(0.2f, 0.2f, 0.2f,
					1.0f, 1.0f, 1.0f,
					1.0f, 1.0f, 1.0f,
					110.0f)
{
	object = new IGraphicalObject;
	mLightSourceData.AddSource(LightSourceData::TypeRepere::CAMERA,
								GL_LIGHT0,
								400.0, 400.0, 400.0,
								0.0, 0.0, 1.0,
								1.0, 1.0, 1.0);
	mLightSourceData.AddSource(LightSourceData::TypeRepere::MONDE,
								GL_LIGHT1,
								-400.0, -400.0, 400.0,
								0.8, 0.0, 0.0,
								1.0, 1.0, 1.0);
}


Scene::~Scene()
{
	delete object;
}

void Scene::draw()
{
	//object.draw();
}

ICamera & Scene::getCamera()
{
	return camera;
}

const ICamera & Scene::getCamera() const
{
	return camera;
}

LightSourceData & Scene::getLightSourceData()
{
	return mLightSourceData;
}

const LightSourceData & Scene::getLightSourceData() const
{
	return mLightSourceData;
}

const Material & Scene::getDefaultMaterial() const
{
	return mDefaultMaterial;
}
