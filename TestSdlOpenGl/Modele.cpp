/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    *
*     http://malgouyres.org                                                    *
*     File: Modele.cpp                                                         *
*                                                                              *
* The program is distributed under the terms of the GNU General Public License *
*                                                                              *
\******************************************************************************/

#include <iostream>
#include "Modele.h"
#include "FramesData.h"
#include "GeometricTransform.h"

const char * textureFilePath = "../textures/caisse2.bmp";

Modele::Modele(uint32_t largeurFenetre, uint32_t hauteurFenetre) :
	mLargeurFenetre(largeurFenetre),
	mHauteurFenetre(hauteurFenetre),
	mNiveauGris(0.0f),
	mDisplayManager(), // Construction du gestionnaire de vue
	mTexture(textureFilePath),
	mAngleRotation(0.0),
	mVitesseRotation(1.0)
{
	position[0] = 0.0;
	position[1] = 0.0;
	position[2] = -2000.0;
	pointDeVisee[0] = 0.0;
	pointDeVisee[1] = 0.0;
	pointDeVisee[2] = 0.0;
	vecteurVertical[0] = 0.0;
	vecteurVertical[1] = 1.0;
	vecteurVertical[2] = 0.0;
	change = 5;
	scene.getCamera().LookAt(position, pointDeVisee, vecteurVertical);
	scene.getCamera().SetProjection(50.0, largeurFenetre / (double)hauteurFenetre, 100.0, 10000.0);
	FramesData::Init(); // Initialisation du compteur de frames
}

float Modele::getNiveauGris() const {
	return mNiveauGris;
}

uint32_t Modele::getLargeurFenetre() const {
	return mLargeurFenetre;
}

uint32_t Modele::getHauteurFenetre() const {
	return mHauteurFenetre;
}

void Modele::Update() {
	// Affichage des Frames par seconde (FPS)
	if (FramesData::Update()) {
		//fprintf(stderr, "%s\n", FramesData::getDescriptionFPS());
		std::cerr << FramesData::getDescriptionFPS() << std::endl;
	}
	// Données d'environnement :
	
	mAngleRotation += mVitesseRotation;
	position[1] -= coef_vertical * mVitesseRotation;
	if (position[1] < -900.0)
		coef_vertical *= -1;
	else if (position[1] > 900.0)
		coef_vertical *= -1;
	scene.getCamera().LookAt(position, pointDeVisee, vecteurVertical);
	
	/*
	position[2] -= change;
	pointDeVisee[1] -= change;
	if (position[2] < 1000.0)
		change = -change;
	else if (position[2] > 10000.0)
		change = -change;
	double tmp = position[2];
	position[2] = 10000.0;
	mCamera.LookAt(position, pointDeVisee, vecteurVertical);
	position[2] = tmp;
	*/
	mDisplayManager.Affichage(*this); // Mise à jour de la vue
}

void Modele::UpdateMouseMotion(int /*xShift*/, int /*yShift*/) {
	// Pas de gestion du mouvement de la souris pour le moment
}

void Modele::Redimensionnement(uint32_t largeurFenetre, uint32_t hauteurFenetre) {
	mLargeurFenetre = largeurFenetre;
	mHauteurFenetre = hauteurFenetre;
	mDisplayManager.setWindowChanged();
}

int32_t Modele::getNbVertices() const
{
	return mNbVertices;
}

int32_t Modele::getNbFaces() const
{
	return mNbFaces;
}

const float * Modele::getVertexPos(int32_t indexVextex) const
{
	return mVertices[indexVextex];
}

const float * Modele::getTexturePos(int32_t indexTexture) const
{
	return mTextures[indexTexture];
}

const int32_t * Modele::getFace(int32_t indexFace) const
{
	return mFaces[indexFace];
}

const int32_t * Modele::getFaceTexture(int32_t indexFace) const
{
	return mFacesTexture[indexFace];
}

const TextureManagerSDL & Modele::getTexture() const
{
	return mTexture;
}

const Material & Modele::getDefaultMaterial() const
{
	return scene.getDefaultMaterial();
}

const ICamera & Modele::getCamera() const
{
	return scene.getCamera();
}

LightSourceData & Modele::getLightSources()
{
	return scene.getLightSourceData();
}

const LightSourceData & Modele::getLightSources() const
{
	return scene.getLightSourceData();
}

void Modele::ApplyModelTransform() const
{
	GeometricTransform::Rotate(mAngleRotation, 0.0, 1.0, 0.0);
}
