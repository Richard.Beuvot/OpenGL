#pragma once
class Material
{
public:
	Material(float ambientR, float ambientG, float ambientB,
			float diffuseR, float diffuseG, float diffuseB,
			float specularR, float specularG, float specularB,
			float shininess);

	const float * getAmbient() const;

	const float * getDiffuse() const;

	const float * getSpecular() const;

	float getShininess() const;
private:
	float mAmbient[4];
	float mDiffuse[4];
	float mSpecular[4];
	float mShininess;
};

