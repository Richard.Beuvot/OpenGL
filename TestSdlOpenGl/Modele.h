/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    *
*     http://malgouyres.org                                                    *
*     File: Modele.h                                                           *
*                                                                              *
* The program is distributed under the terms of the GNU General Public License *
*                                                                              *
\******************************************************************************/

#ifndef HEADER_MODELE_H
#define HEADER_MODELE_H

#include "DisplayManager.h"
#include "ICamera.h"
#include "Scene.h"
#include "TextureManagerSDL.h"
#include <cstdint>
/**
 * Modèle de données
 */
class Modele {

public:
	/** @brief  Constructeur prenant en paramètre la géométrie de la fenêtre
	 * Initialise les données de l'application et données nécessaires à l'affichage.
	 * @param largeurFenetre largeur de la fenêtre graphique en pixels
	 * @param hauteurFenetre hauteur de la fenêtre graphique en pixels
	 **/
	Modele(uint32_t largeurFenetre, uint32_t hauteurFenetre);

	// //////// Accesseurs
	float getNiveauGris() const;
	uint32_t getLargeurFenetre() const;
	uint32_t getHauteurFenetre() const;

	/** Mise à jour du modèle invoquée à chaque événement timer */
	void Update();
	/** Mise à jour du modèle invoquée à chaque événement souris
	 * @param xShift Variation de la coordonnée x de la souris
	 * @param yShift Variation de la coordonnée y de la souris
	 **/
	void UpdateMouseMotion(int xShift, int yShift);

	/** @brief Réglage du cadrage pour la vue
	 * Doit être rappelée si la taille de la vue change (SDL_WINDOWEVENT)
	 * @param largeurFenetre largeur de la fenêtre graphique en pixels
	 * @param hauteurFenetre hauteur de la fenêtre graphique en pixels
	 */
	void Redimensionnement(uint32_t largeurFenetre, uint32_t hauteurFenetre);

	int32_t getNbVertices() const;
	int32_t getNbFaces() const;
	const float* getVertexPos(int32_t) const;
	const float* getTexturePos(int32_t) const;
	const int32_t* getFace(int32_t) const;
	const int32_t* getFaceTexture(int32_t) const;

	const TextureManagerSDL& getTexture() const;

	const Material& getDefaultMaterial() const;

	const ICamera& getCamera() const;

	LightSourceData& getLightSources();
	const LightSourceData& getLightSources() const;

	void ApplyModelTransform() const;


private:
	/** largeur de la fenêtre graphique en pixels */
	uint32_t mLargeurFenetre;
	/** hauteur de la fenêtre graphique en pixels */
	uint32_t mHauteurFenetre;
	/** Niveau de gris du fond */
	float mNiveauGris;
	/** Gestionnaire de la vue (affichage) */
	DisplayManager mDisplayManager;

	const int32_t mNbVertices = 8;
	const int32_t mNbFaces = 12;
	const float mVertices[8][3] = {
		{-350.0, -350.0, -350.0},
		{-350.0, 350.0, -350.0},
		{350.0, -350.0, -350.0},
		{350.0, 350.0, -350.0},

		{-350.0, -350.0, 350.0},
		{-350.0, 350.0, 350.0},
		{350.0, -350.0, 350.0},
		{350.0, 350.0, 350.0}
	};
	const int32_t mFaces[12][3] = {
		{0, 6, 2},
		{0, 4, 6},

		{0, 3, 1},
		{0, 2, 3},

		{0, 1, 5},
		{0, 5, 4},

		{7, 1, 3},
		{7, 5, 1},

		{7, 3, 2},
		{7, 2, 6},

		{7, 4, 5},
		{7, 6, 4}
	};

	const int32_t mFacesTexture[12][3] = {
		{0, 2, 1},
		{0, 3, 2},

		{0, 2, 1},
		{0, 3, 2},

		{0, 3, 2},
		{0, 2, 1},

		{0, 2, 1},
		{0, 3, 2},

		{0, 3, 2},
		{0, 2, 1},

		{0, 2, 1},
		{0, 3, 2}
	};

	const float mTextures[4][2] = {
		{0.0, 0.0},
		{0.0, 1.0},
		{1.0, 1.0},
		{1.0, 0.0}
	};

	TextureManagerSDL mTexture;
	double mAngleRotation;
	double mVitesseRotation;
	Scene scene;
	double position[3];
	double pointDeVisee[3];
	double vecteurVertical[3];
	double change;
	float coef_vertical = 35.0;
};
#endif
