#pragma once

#include <vector>
#include "Point.h"
#include "Face.h"
#include "Vect3d.h"

class IGraphicalObject
{
public:

	class iterator
	{
	public :
		iterator();
		void next();
		//const IGraphicalObject & getObject() const;
		IGraphicalObject & getObject();
		//dereferencement

	private:
		std::vector < std::vector<IGraphicalObject>::iterator > iterators ;
		std::vector < std::vector<IGraphicalObject>* > vectors;
		// Matrice de proj, de vue , etc
		std::vector < Vect3d > positions;
		std::vector < Vect3d > angles;
		std::vector < Vect3d > rotations;
	};

	IGraphicalObject();
	~IGraphicalObject();

	iterator begin();
	iterator end();

private:

	Vect3d position;
	Vect3d angles;
	Vect3d rotations;

	// A remove
	std::vector<Point> vertices;
	std::vector<Face> faces;
};

