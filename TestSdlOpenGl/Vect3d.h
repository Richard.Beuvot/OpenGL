#pragma once
class Vect3d
{
public:
	Vect3d(double x1 = 0.0, double x2 = 0.0, double x3 = 0.0) { v[0] = x1; v[1] = x2; v[2] = x3; }
	~Vect3d() {}

	double& at(unsigned i) { return v[i]; }
	const double& at(unsigned i) const { return v[i]; }

	double& operator[](unsigned i) { return at(i); }
	const double& operator[](unsigned i) const { return at(i); }

private:
	double v[3];
};

