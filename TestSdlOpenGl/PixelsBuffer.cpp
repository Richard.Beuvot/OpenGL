#include "PixelsBuffer.h"

PixelsBuffer::PixelsBuffer():
	mWidth(0),
	mHeight(0),
	mBytesPerPixel(0),
	mScanLineWidth(0),
	mPixels()
{}


unsigned char * PixelsBuffer::GetRawData()
{
	return mPixels.data();
}

PixelsBuffer::~PixelsBuffer()
{
}

PixelsBuffer::PixelsBuffer(int width, int height, int bytesPerPixel) :
	mWidth(width),
	mHeight(height),
	mBytesPerPixel(bytesPerPixel),
	mScanLineWidth(width * bytesPerPixel + (4 - (width * bytesPerPixel) % 4) % 4),
	mPixels(mScanLineWidth * height, 0xff)
{}

PixelsBuffer::PixelsBuffer(int width, int height, int bytesPerPixel, unsigned char *data) :
	mWidth(width),
	mHeight(height),
	mBytesPerPixel(bytesPerPixel),
	mScanLineWidth(width * bytesPerPixel + (4 - (width * bytesPerPixel) % 4) % 4),
	mPixels(mScanLineWidth * height)
{
	memcpy(mPixels.data(), data, mScanLineWidth * height);
}

void PixelsBuffer::setSize(int width, int height, int bytesPerPixel)
{
	mWidth = width;
	mHeight = height;
	mBytesPerPixel = bytesPerPixel;
	mScanLineWidth = width * bytesPerPixel + (4 - (width * bytesPerPixel) % 4) % 4;
	mPixels = std::vector<unsigned char>(mScanLineWidth * height, 0xff);
}

int PixelsBuffer::GetWidth()
{
	return mWidth;
}

int PixelsBuffer::GetHeight()
{
	return mHeight;
}

int PixelsBuffer::GetBytesPerPixel()
{
	return mBytesPerPixel;
}

void PixelsBuffer::SetPixelPerBytes(int x, int y, unsigned char *array)
{
	int startByte = x * mScanLineWidth + y * mBytesPerPixel;
	memcpy(mPixels.data() + startByte, array, mBytesPerPixel);
}
