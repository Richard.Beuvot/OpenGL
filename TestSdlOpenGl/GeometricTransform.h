/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: GeometricTransform.h                                               * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#ifndef HEADER_GEOMETRIC_TRANSFORM_H
#define HEADER_GEOMETRIC_TRANSFORM_H

#include <cstdlib>
#include <cstdint>
/**
 * GESTION DES TRANSFORMATIONS GEOMETRIQUE (part of : GL WRAPPER)
 */
struct GeometricTransform{
  /** @brief Applique un re-cadrage 2D par affinités orthogonales et translation
   * @param viewCenterX coordonnée x du centre du rectangle dans le plan qui
   * est mappé sur la fenêtre graphique pour être affiché.
   * @param viewCenterY coordonnée u du centre du rectangle dans le plan qui
   * est mappé sur la fenêtre graphique pour être affiché.
   **/
  static void Viewport(int32_t viewCenterX,  int32_t viewCenterY,
                       uint32_t viewWidth, uint32_t viewHeight);

 /** @brief Applique la nouvelle projection en perspectives sur les primitives graphiques
  * par multiplication de la matrice courante
  * Seuls les objets affichés ultérieurement sont affectés */
  static void ApplyPerspectiveProjection(double angleOuvertureY, double aspect,
                                         double zProche, double zEloigne);
  
  /** Réinitialise la transformation ModelView à l'identité */
  static void ClearModelView();
  
  /** Réinitialise la transformation Projection à l'identité */
  static void ClearProjection();
  
  /** Redéfinit la position et l'orientation de la caméra */
  static void LookAt(const double position[3], const double pointDeVisee[3],
                     const double vecteurVertical[3]);
  
  /** Applique une translation d'un vecteur */
  static void Translate(double vecX, double vecY, double vecZ);
  
  /** Applique une rotation autour de l'axe passant par O dirigé par un vecteur */
  static void Rotate(double vecX, double vecY, double vecZ, double angle);
  
  /** Applique un changement d'échelle (affinités orthogonales) sur chaque axe */
  static void Scale(double factorX, double factorY, double factorZ);
};
#endif
