/******************************************************************************\
*     Copyright (C) 2017 by Rémy Malgouyres                                    * 
*     http://malgouyres.org                                                    * 
*     File: WrapperSDL.cpp                                                     * 
*                                                                              * 
* The program is distributed under the terms of the GNU General Public License * 
*                                                                              * 
\******************************************************************************/ 

#include <Windows.h>
#include <GL/glew.h>
#include <GL/GL.h>
//#include <GL/glut.h>

#include "WrapperSDL.h"

WrapperSDL::WrapperSDL(uint32_t largeurFenetreInit, uint32_t hauteurFenetreInit,
                       const char* windowTitle, int argc, char**argv):
        mWindowManager(largeurFenetreInit, hauteurFenetreInit, windowTitle),
        mModele(largeurFenetreInit, hauteurFenetreInit)
{
  //glutInit(&argc, argv); // Initialisation de la GLUT
  EventController::Init(mWindowManager.getP_Window(), &mModele);
}

uint32_t WrapperSDL::getTicks() {
  return SDL_GetTicks();
}
