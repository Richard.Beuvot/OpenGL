#include "TextureManagerSDL.h"

#include <Windows.h>
#include <GL/glew.h>
#include <GL/GL.h>


TextureManagerSDL::TextureManagerSDL() :
	mTextId(0),
	mBuffer(0, 0, 0)
{}

TextureManagerSDL::TextureManagerSDL(const char * pathToBMP) :
	mTextId(0),
	mBuffer(0, 0, 0)
{
	CreeBufferFromPicture(pathToBMP);
	CreeTexture2D();
}


TextureManagerSDL::~TextureManagerSDL()
{
	glDeleteTextures(1, &mTextId);
}

void TextureManagerSDL::SelectTexture2D() const
{
	glBindTexture(GL_TEXTURE_2D, mTextId);
}

void TextureManagerSDL::CreeBufferFromPicture(const char * pathToBMP)
{
	SDL_Surface * surface = CreeSurface(pathToBMP);
	mBuffer = PixelsBuffer(surface->w, surface->h, surface->format->BytesPerPixel, (uint8_t *)surface->pixels);
	SDL_FreeSurface(surface);
}

void TextureManagerSDL::CreeTexture2D()
{
	glDeleteTextures(1, &mTextId);

	glGenTextures(1, &mTextId);
	SelectTexture2D();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	GLint mode;
	switch (mBuffer.GetBytesPerPixel())
	{
		case 1:
			mode = GL_LUMINANCE;
			break;
		case 3:
			mode = GL_RGB;
			break;
		case 4:
			mode = GL_RGBA;
			break;
		default:
			//error
			break;
	}
	glTexImage2D(GL_TEXTURE_2D, 0, mode, mBuffer.GetWidth(), mBuffer.GetHeight(), 0, mode, GL_UNSIGNED_BYTE, mBuffer.GetRawData());
	glBindTexture(GL_TEXTURE_2D, mTextId);
}

SDL_Surface * TextureManagerSDL::CreeSurface(const char * pathToBMP)
{
	if (strlen(pathToBMP)
		|| (strcmp(".bmp", pathToBMP + (strlen(pathToBMP) - 4)) &&
			strcmp(".BMP", pathToBMP + (strlen(pathToBMP) - 4))))
	{
		//error
	}
	SDL_Surface * surface = SDL_LoadBMP(pathToBMP);
	if (!surface)
	{
		//error
	}
	return surface;
}
