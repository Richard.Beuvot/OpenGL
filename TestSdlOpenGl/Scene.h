#ifndef SCENE_H
#define SCENE_H

#include "Material.h"
#include "ICamera.h"
#include "IGraphicalObject.h"
#include "LightSourceData.h"

class Scene
{
public:
	Scene();
	~Scene();

	void draw();
	ICamera& getCamera();
	const ICamera& getCamera() const;

	LightSourceData& getLightSourceData();
	const LightSourceData& getLightSourceData() const;

	const Material& getDefaultMaterial() const;

private:
	Material mDefaultMaterial;
	LightSourceData mLightSourceData;
	ICamera camera;
	IGraphicalObject * object;
};

#endif