#pragma once

#include <cstdint>
#include <SDL.h>
#include "PixelsBuffer.h"

class TextureManagerSDL
{
public:
	TextureManagerSDL();
	TextureManagerSDL(const char *);
	~TextureManagerSDL();

	void SelectTexture2D() const;
	void CreeBufferFromPicture(const char *);
	void CreeTexture2D();

private:
	SDL_Surface* CreeSurface(const char *);

private:

	uint32_t mTextId;
	PixelsBuffer mBuffer;
};

