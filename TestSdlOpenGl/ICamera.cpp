#include <cstring>
#include "ICamera.h"
#include "GeometricTransform.h"

ICamera::ICamera(double angleOuvertureY, double aspect,
				double z_proche, double z_eloigne,
				double positionX, double positionY, double positionZ,
				double pointDeViseeX, double pointDeViseeY, double pointDeViseeZ,
				double vecteurVerticalX, double vecteurVerticalY, double vecteurVerticalZ) :
	mAngleOuvertureY(angleOuvertureY),
	mAspect(aspect),
	mZ_proche(z_proche),
	mZ_eloigne(z_eloigne)
{
	mPosition[0] = positionX;
	mPosition[1] = positionY;
	mPosition[2] = positionZ;
	
	mPointDeVisee[0] = pointDeViseeX;
	mPointDeVisee[1] = pointDeViseeY;
	mPointDeVisee[2] = pointDeViseeZ;
	
	mVecteurVertical[0] = vecteurVerticalX;
	mVecteurVertical[1] = vecteurVerticalY;
	mVecteurVertical[2] = vecteurVerticalZ;
}


void ICamera::LookAt(double position[3], double pointDeVisee[3], double vecteurVertical[3])
{
	memcpy(mPosition, position, 3 * sizeof(double));
	memcpy(mPointDeVisee, pointDeVisee, 3 * sizeof(double));
	memcpy(mVecteurVertical, vecteurVertical, 3 * sizeof(double));
}

void ICamera::SetProjection(double angleOuvertureY, double aspect, double z_proche, double z_eloigne)
{
	mAngleOuvertureY = angleOuvertureY;
	mAspect = aspect;
	mZ_proche = z_proche;
	mZ_eloigne = z_eloigne;
}

		
void ICamera::ApplyPerspectiveProjection() const
{
	GeometricTransform::ApplyPerspectiveProjection(mAngleOuvertureY, mAspect, mZ_proche, mZ_eloigne);
}

void ICamera::ClearModelView()
{
	GeometricTransform::ClearModelView();
}

void ICamera::ClearProjection()
{
	GeometricTransform::ClearProjection();
}

void ICamera::ApplyCameraCoordinates() const
{
	GeometricTransform::LookAt(mPosition, mPointDeVisee, mVecteurVertical);
}

void ICamera::UpdateAspect(double aspect)
{
	SetProjection(mAngleOuvertureY, aspect, mZ_proche, mZ_eloigne);
}
