#ifndef ICAMERA_H
#define ICAMERA_H

class ICamera
{
	public:
		ICamera(double = 0.0, double = 0.0, double = 0.0, double = 0.0, double = 55.0, double = 55.0, double = 0.0, double = 0.0, double = 0.0, double = 0.0, double = 0.0, double = 0.0, double = 0.0);
		
		// A mettre dans l'objet herite plus tard
		void LookAt(double [3], double [3], double [3]);
		void SetProjection(double , double , double , double );
		
		void ApplyPerspectiveProjection() const;
		void ApplyCameraCoordinates() const;
		void UpdateAspect(double);
		
		static void ClearModelView();
		static void ClearProjection();
		
	private:
		double mAngleOuvertureY;
		double mAspect;
		double mZ_proche;
		double mZ_eloigne;
		
		double mPosition[3];
		double mPointDeVisee[3];
		double mVecteurVertical[3];
		
};

#endif
