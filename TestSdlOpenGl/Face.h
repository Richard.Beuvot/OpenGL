#pragma once

#include <cstdint>

class Face
{
public:

	Face(int32_t = 0, int32_t = 0, int32_t = 0);

	int32_t Index1;
	int32_t Index2;
	int32_t Index3;
};