#pragma once

class Material;
class Modele;

class RenderingGlobalRoutine
{
public:
	static void Init();

	static void InitView();

	static void ApplyPointLightPosition(int lightId, const float position[4]);

	static void ApplyPointLightIntensity(int lightId, const float diffuseIntensity[4], const float specularIntensity[4]);

	static void DisablePointLight(int lightId);

	static void ApplyMaterial(Material material);

	static void DrawModel(const Modele&);
};

